FactoryBot.define do
  factory :author do
    name { 'An Author' }
  end
end
