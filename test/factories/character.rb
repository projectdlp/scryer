FactoryBot.define do
  factory :character do
    fandom

    isfemale { nil }

    factory :character_m do
      isfemale { false }
    end

    factory :character_f do
      isfemale { true }
    end
  end
end
