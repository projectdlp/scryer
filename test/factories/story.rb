FactoryBot.define do
  factory :story do
    # author
    title { "My Little Story" }

    characters { [] }

    fandoms {
      create_list(:fandom, 2)
    }

    summary { "" }

    transient do
      ships { [] }
    end

    after(:build) do |story, evaluator|
      story[:relationships] = evaluator.ships.map do |ship|
        {
            characters: ship.map { |s| { id: s.id, name: s.name } }
        }
      end
    end

    association :author, strategy: :build
  end
end
