require 'test_helper'
require 'scryer/ffn/metadata_parser'

class MetadataParserTest < ActiveSupport::TestCase
  make_my_diffs_pretty!

  test 'handles_well_formed_input_1' do
    f = Fandom.new(id: 224, name: 'Harry Potter')
    f.save!
    Character.new(id: 20, name: 'Harry P.', fandom: f).save!
    Character.new(id: 21, name: 'Sirius B.', fandom: f).save!
    Character.new(id: 22, name: 'Remus L.', fandom: f).save!

    str = 'Rated: T - French - Chapters: 13 - Words: 69,618 - Reviews: 265 - Favs: 132 - Follows: 114 - Updated: - Published: - Harry P., Sirius B., Remus L.'

    expected = Scryer::FFN::Metadata.new(
      characters: [
        {
          id: 20,
          name: 'Harry P.'
        },
        {
          id: 21,
          name: 'Sirius B.'
        },
        {
          id: 22,
          name: 'Remus L.'
        }
      ],
      chapters: 13,
      favs: 132,
      follows: 114,
      rated: 'T',
      reviews: 265,
      language: 'French',
      words: 69_618
    )

    actual = Scryer::FFN::MetadataParser.parse_info_string(str, [224])

    assert_equal expected, actual
  end
  test 'handles_well_formed_input_2' do
    f = Fandom.new(id: 2927, name: 'Mass Effect')
    f.save!
    Character.new(id: 66_813, name: 'Shepard (M)', fandom: f).save!
    Character.new(id: 37_718, name: 'Miranda L.', fandom: f).save!
    Character.new(id:16_082, name: 'Garrus V.', fandom: f).save!

    str = 'Rated: M - English - Adventure/Romance - Chapters: 100 - Words: 1,404,198 - Reviews: 502 - Favs: 464 - Follows: 429 - Updated: - Published: - [Shepard (M), Miranda L.] Garrus V. - Complete'

    expected = Scryer::FFN::Metadata.new(
      categories: [
        {
          id: 6,
          name: 'Adventure'
        },
        {
          id: 2,
          name: 'Romance'
        }
      ],
      characters: [
        {
          id: 66_813,
          name: 'Shepard (M)'
        },
        {
          id: 37_718,
          name: 'Miranda L.'
        },
        {
          id: 16_082,
          name: 'Garrus V.'
        },
      ],
      chapters: 100,
      favs: 464,
      follows: 429,
      rated: 'M',
      relationships: [
        {
          characters: [
            {
              id: 66_813,
              name: 'Shepard (M)'
            },
            {
              id: 37_718,
              name: 'Miranda L.'
            },
          ]
        }
      ],
      reviews: 502,
      status: 'Complete',
      language: 'English',
      words: 1_404_198
    )

    actual = Scryer::FFN::MetadataParser.parse_info_string(str, [2927])

    assert_equal expected, actual
  end
  test 'handles_story_without_categories_but_category_name_in_characters' do
    f = Fandom.new(id: 8, name: 'Star Wars')
    f.save!
    Character.new(id: 66_813, name: 'Darth Vader', fandom: f).save!
    Character.new(id: 37_718, name: 'Jango Fett', fandom: f).save!
    Character.new(id:16_082, name: 'Darth Maul', fandom: f).save!
    Character.new(id: 1234, name: 'General Grievous', fandom: f).save!

    str = 'Rated: M - English - Chapters: 1 - Words: 33 - Published: - Darth Vader, Jango Fett, General Grievous, Darth Maul'

    expected = Scryer::FFN::Metadata.new(
      categories: [],
      characters: [
        {
          id: 66_813,
          name: 'Darth Vader'
        },
        {
          id: 37_718,
          name: 'Jango Fett'
        },
        {
          id: 1_234,
          name: 'General Grievous'
        },
        {
          id: 16_082,
          name: 'Darth Maul'
        },
      ],
      chapters: 1,
      favs: 0,
      follows: 0,
      rated: 'M',
      relationships: [],
      reviews: 0,
      status: 'In-Progress',
      language: 'English',
      words: 33
    )

    actual = Scryer::FFN::MetadataParser.parse_info_string(str, [8])

    assert_equal expected, actual
  end
  test 'handles_story_with_id' do
    f = Fandom.new(id: 8, name: 'Star Wars')
    f.save!
    Character.new(id: 66_813, name: 'Darth Vader', fandom: f).save!
    Character.new(id: 37_718, name: 'Jango Fett', fandom: f).save!
    Character.new(id: 16_082, name: 'Darth Maul', fandom: f).save!
    Character.new(id: 1234, name: 'General Grievous', fandom: f).save!

    str = 'Rated: M - English - Chapters: 1 - Words: 33 - Published: - Darth Vader, Jango Fett, General Grievous, Darth Maul - id: 9704180 '

    expected = Scryer::FFN::Metadata.new(
      categories: [],
      characters: [
        {
          id: 66_813,
          name: 'Darth Vader'
        },
        {
          id: 37_718,
          name: 'Jango Fett'
        },
        {
          id: 1_234,
          name: 'General Grievous'
        },
        {
          id: 16_082,
          name: 'Darth Maul'
        },
      ],
      chapters: 1,
      favs: 0,
      follows: 0,
      rated: 'M',
      relationships: [],
      reviews: 0,
      status: 'In-Progress',
      language: 'English',
      words: 33
    )

    actual = Scryer::FFN::MetadataParser.parse_info_string(str, [8])

    assert_equal expected, actual
  end
end
