require 'test_helper'
require 'scryer/tagging/content_tagger'

class ContentTaggerTest < ActiveSupport::TestCase
  test 'marks_slash_story_from_characters' do
    harry = create(:character_m, name: 'Harry P.')
    voldemort = create(:character_m, name: 'Voldemort')

    story = build(:story, characters: [harry.id, voldemort.id], ships: [
        [harry, voldemort]
    ])

    assert_equal [:slash], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'marks_femslash_story_from_characters' do
    hermione = create(:character_f, name: 'Hermione G.')
    ginny = create(:character_f, name: 'Ginny W.')

    story = build(:story, characters: [hermione.id, ginny.id], ships: [
        [hermione, ginny]
    ])

    assert_equal [:femslash], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'flips_genders_for_fem_prefix' do
    harry = create(:character_m, name: 'Harry P.')
    voldemort = create(:character_m, name: 'Voldemort')

    story = build(:story, ships: [
        [harry, voldemort]
    ], characters: [harry.id, voldemort.id], summary: 'Hello World! Fem!Creature!Voldemort')

    assert_equal [:gender_bender], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'flips_genders_for_fem_prefix_2' do
    harry = create(:character_m, name: 'Harry P.')
    charlie = create(:character_m, name: 'Charlie W.')

    story = build(:story,
                  ships: [[harry, charlie]],
                  characters: [harry.id, charlie.id],
                  summary: 'Charlie Weasley never believed in chance, until he met Emily Potter that was. For the golden snitch. Fem!harry! fem!harry/Charlie Weasley')

    assert_equal [:gender_bender], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'gender bender from male!' do
    harry = create(:character_m, name: 'Harry P.')
    hermione = create(:character_f, name: 'Hermione G.')

    story = build(:story, characters: [harry.id, hermione.id], summary: 'Hello World! Male!Hermione')

    assert_equal [:gender_bender], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'no gender bender from characters of unknown gender' do
    foo = create(:character, name: 'Foo')
    bar = create(:character, name: 'Bar')

    story = build(:story, characters: [foo.id, bar.id], summary: 'Fem!Foo Male!Bar')

    assert_equal [], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'flips_genders_for_male_prefix' do
    harry = create(:character_m, name: 'Harry P.')
    hermione = create(:character_f, name: 'Hermione G.')

    story = build(:story, ships: [
        [harry, hermione]
    ], characters: [harry.id, hermione.id], summary: 'Hello World! Male!Hermione')

    assert_equal [:slash, :gender_bender], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'slash_in_summary_adds_tag' do
    harry = create(:character_m, name: 'Harry P.')

    story = build(:story, ships: [
        [harry]
    ], characters: [harry.id], summary: 'This is a great story, slash!')

    assert_equal [:slash], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'not_slash_no_tag' do
    harry = create(:character_m, name: 'Harry P.')

    story = build(:story, ships: [
        [harry]
    ], characters: [harry.id], summary: 'This is a great story, no slash!')

    assert_equal [], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'femslash_in_summary_adds_tag' do
    harry = create(:character_m, name: 'Harry P.')

    story = build(:story, ships: [
        [harry]
    ], characters: [harry.id], summary: 'This is a great story, femslash!')

    assert_equal [:femslash], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'not_femslash_no_tag' do
    harry = create(:character_m, name: 'Harry P.')

    story = build(:story, ships: [
        [harry]
    ], characters: [harry.id], summary: 'This is a great story, no femslash!')

    assert_equal [], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'multi_from_ships' do
    harry = create(:character_m, name: 'Harry P.')
    hermione = create(:character_f, name: 'Hermione G.')
    ginny = create(:character_f, name: 'Ginny W.')

    story = build(:story, ships: [
        [harry, hermione, ginny]
    ], characters: [harry.id, hermione.id, ginny.id], summary: 'Foo')

    assert_equal [:multi, :femslash], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'multi_from_summary' do
    harry = create(:character_m, name: 'Harry P.')
    hermione = create(:character_f, name: 'Hermione G.')
    ginny = create(:character_f, name: 'Ginny W.')

    %w(HarryXMulti Harry/Multi).each do |summary|
      story = build(:story, ships: [], characters: [harry.id, hermione.id, ginny.id], summary: summary)
      assert_equal [:multi], Scryer::Tagging::ContentTagger::generate_story_tags(story)
    end
  end

  test 'harem_from_ships' do
    harry = create(:character_m, name: 'Harry P.')
    hermione = create(:character_f, name: 'Hermione G.')
    ginny = create(:character_f, name: 'Ginny W.')
    luna = create(:character_f, name: 'Luna L.')

    story = build(:story, ships: [
        [harry, hermione, ginny, luna]
    ], characters: [harry.id, hermione.id, ginny.id, luna.id], summary: 'Foo')

    assert_equal [:harem, :femslash], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'harem_from_summary' do
    harry = create(:character_m, name: 'Harry P.')

    story = build(:story, ships: [
        [harry]
    ], characters: [harry.id], summary: 'Blah blah harem')

    assert_equal [:harem], Scryer::Tagging::ContentTagger::generate_story_tags(story)
  end

  test 'extracts_all_tags' do
    harry = create(:character_m, name: 'Harry P.')
    voldemort = create(:character_m, name: 'Voldemort')
    hermione = create(:character_f, name: 'Hermione G.')
    ron = create(:character_m, name: 'Ron W.')
    ginny = create(:character_f, name: 'Ginny W.')

    summary = 'Foo bar Fem!Harry Fem!Creature!Voldemort Grey!Harry Male!Hermione Female!Ron Guy!Ginny blah blah'
    story = build(:story, characters: [
        [harry.id, voldemort.id, hermione.id, ron.id, ginny.id]
    ], summary: summary)


    expected = {
        harry => Set.new([:female, :grey]),
        voldemort => Set.new([:creature, :female]),
        hermione => Set.new([:male]),
        ron => Set.new([:female]),
        ginny => Set.new([:male])
    }

    assert_equal expected, Scryer::Tagging::ContentTagger::extract_character_tags(story)
  end
end
