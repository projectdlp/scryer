const { environment } = require('@rails/webpacker')
environment.config.set('resolve.alias', {jquery: 'jquery/src/jquery'});

const webpack = require('webpack')
environment.plugins.prepend('Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    Popper: ['popper.js', 'default'],
    Rails: ['@rails/ujs'],
    timeago: 'timeago',
  })
)

environment.loaders.get('sass').use.splice(-1, 0, {
  loader: 'resolve-url-loader'
});

module.exports = environment
