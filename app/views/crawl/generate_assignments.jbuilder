json.array! @assignments do |assignment|
  json.(assignment, :fandoms, :last_crawled, :should_crawl, :should_recrawl)
end
