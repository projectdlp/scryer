require 'set'

class StoryFetchContext
  def initialize(current_user, story_ids)
    @current_user = current_user
    @stories = from_story_ids(story_ids)

    @opened_stories = from_search_results do
      @current_user.story_clicks.where(story_id: story_ids)
    end

    # @read_stories = from_search_results(lambda { |r| r.max_chapter }) do
    #   PensieveEvent
    #       .progress(current_user, story_ids)
    # end
    @read_stories = {}

    @queued_stories = from_search_results do
      @current_user.queue_entries.active.where(story_id: story_ids)
    end

    @categories = Category.where(id: @stories.inject(Set.new) { |acc, s| acc.merge(s.category_ids) }.to_a).index_by(&:id)
    @characters = Character.where(id: @stories.inject(Set.new) { |acc, s| acc.merge(s.character_ids) }.to_a).index_by(&:id)
    @fandoms = Fandom.where(id: @stories.inject(Set.new) { |acc, s| acc.merge(s.fandom_ids) }.to_a).index_by(&:id)
  end

  def categories(story)
    story.category_ids.map { |id| @categories[id] }.compact
  end

  def characters(story)
    story.character_ids.map { |id| @characters[id] }.compact
  end

  def fandoms(story)
    story.fandom_ids.map { |id| @fandoms[id] }.compact
  end

  # @param [Story] story
  # @return [Array<Array<Character>>]
  def relationships(story)
    relations = story.relationships_raw

    unless relations && relations.size > 0
      return []
    end

    if !relations[0].is_a?(Array) && relations[0].has_key?('characters')
      relations = relations.map { |g| g['characters'].map { |c| {'character_id' => c['id']} } }
    end

    relations.map do |group|
      group.map { |character| character['character_id'].to_i }
          .map { |id| @characters[id] }
    end || []
  end

  def opened?(story)
    @opened_stories[story.id]
  end

  def read?(story)
    @read_stories[story.id]
  end

  def queued?(story)
    @queued_stories[story.id]
  end

  def stories
    @stories
  end

  private

  def from_story_ids(story_ids)
    stories_by_id =
        Story.where(id: story_ids)
            .includes([:author, :story_to_thread])
            .joins(:author)
            .left_joins(:story_to_thread)
            .index_by(&:id)

    story_ids.map { |id| stories_by_id[id] }.compact
  end


  def from_search_results(result_transform = lambda { |r| r }, &block)
    unless @current_user
      return {}
    end

    block.call
        .to_a
        .inject({}) do |r, result|
      r[result.story_id] = result_transform.call(result)
      r
    end
  end
end
