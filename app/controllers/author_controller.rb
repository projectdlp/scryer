class AuthorController < ApplicationController
  def index
    @author = Author.find(params[:author_id])
    story_ids = @author.stories.order('updated DESC').map(&:id)
    @story_context = StoryFetchContext.new(current_user, story_ids)
    @stories = @story_context.stories
  end
end
