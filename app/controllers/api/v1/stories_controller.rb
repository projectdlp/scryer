class Api::V1::StoriesController < ApplicationController
    include Cors
  skip_before_action :verify_authenticity_token
  before_action :set_story, only: [:show]
  before_action :cors_preflight_check
  after_action :cors_headers

  respond_to :json

  def show
    if @story.nil?
      render json: {status: 'not_found', message: "Story(#{@id}) not found and couldn't be retrieved." }, status: 404
    else
      respond_with(@story)
    end
  end

  private
    def set_story
      @id = params[:id]
      @story = Story.find_by(id: @id)

      if @story == nil
        FetchAndIndexWorker.new.perform(@id)
        @story = Story.find_by(id: @id)
      elsif @story.last_seen < 2.weeks.ago
        FetchAndIndexWorker.perform_async(@id)
      end
    end

    def story_params
      params.require(:story).permit(:show)
    end
end
