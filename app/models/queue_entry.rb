require 'scryer/constants'

class QueueEntry < ApplicationRecord
  validates :origin, inclusion: { in: Scryer::Constants::ORIGIN }, allow_nil: true

  paginates_per 25
  scope :active, -> { where(deleted_at: nil).order('updated_at asc') }

  belongs_to :story, -> { includes :author }
  belongs_to :user
  counter_culture :user, column_name: proc {|model| model.deleted_at.nil? ? 'queue_entries_count' : nil },
                  column_names: {
                    ["queue_entries.deleted_at IS NULL"] => 'queue_entries_count',
                  }

  def soft_delete
    self.deleted_at = DateTime.now
    save!
  end
end
