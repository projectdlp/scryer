require 'scryer/constants'
require 'securerandom'

class User < ActiveRecord::Base
  before_create :set_auth_token

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :username, :presence => true, :uniqueness => true, length: 2..64
  validate :username_not_obviously_spam
  validate :email_not_banned
  validate :email_not_obviously_spam

  has_many :feedbacks
  has_many :favorites
  has_many :hidden_authors
  has_many :hidden_stories
  has_many :saved_searches
  has_many :story_clicks
  has_many :pensieve_events
  has_many :queue_entries

  def to_s
    username
  end

  def admin?
    id == 1
  end

private
  def set_auth_token
    return if auth_token.present?
    self.auth_token = SecureRandom.uuid.gsub(/\-/, '')
  end

  def username_not_obviously_spam
    if username.present?
      if username.strip.match?(/http(s)?:\/\//)
        errors.add(:email, "username not permitted")
      end
    end
  end

  def email_not_banned
    if email.present? && origin != "sso"
      if Scryer::Constants::BANNED_EMAIL_PATTERNS.any? { |pattern| email.strip.match?(pattern) }
        errors.add(:email, "email address not permitted")
      end
    end
  end

  def email_not_obviously_spam
    if email.present?
      if email.strip.split('@')[0].count('.') > 2
        errors.add(:email, "email address not permitted")
      end

      # It has to look somewhat like an email.
      unless email.strip.match?(/^[0-9a-zA-Z.+_-]+@[-0-9a-zA-Z.+_-]+/)
        errors.add(:email, "email address not permitted")
      end
    end
  end
end
