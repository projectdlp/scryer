class StoryToThread < ActiveRecord::Base
  self.primary_key = 'story_id'
  self.table_name = 'story_to_thread'

  has_one :story

  def score
    if vote_count.nil? or vote_total.nil?
      return 0
    end

    vote_count.to_f / vote_total
  end
end
