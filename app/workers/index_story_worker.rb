class IndexStoryWorker < StoryIndexBase
  sidekiq_options :backtrace => true

  def perform(story_id, body)
    logger.info "[#{story_id}] Indexing story from Crawler"
    $statsd.increment 'index.story.count'

    start_time = Time.now
    index(story_id, body, true)
    $statsd.timing('index.story.latency', (Time.now - start_time)*1000)
  end
end
