class CreateFandomCategories < ActiveRecord::Migration[4.2]
  def change
    create_table :fandom_categories do |t|
      t.string :name
      t.string :slug

      t.timestamps null: false
    end
  end
end
