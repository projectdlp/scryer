class AddReadFilterToSearch < ActiveRecord::Migration[5.1]
  def change
    add_column :searches, :read_filter, :string
  end
end
