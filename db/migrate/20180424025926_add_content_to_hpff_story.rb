class AddContentToHpffStory < ActiveRecord::Migration[5.1]
  def change
    add_column :hpff_stories, :content, :text
  end
end
