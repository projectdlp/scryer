class CreateFandoms < ActiveRecord::Migration[4.2]
  def change
    create_table :fandoms do |t|
      t.string :name

      t.timestamps
    end
  end
end
