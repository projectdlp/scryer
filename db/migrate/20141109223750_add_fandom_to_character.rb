class AddFandomToCharacter < ActiveRecord::Migration[4.2]
  def change
    add_column :characters, :fandom_id, :integer
  end
end
