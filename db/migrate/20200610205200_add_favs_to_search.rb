class AddFavsToSearch < ActiveRecord::Migration[6.0]
	def change
	  add_column :searches, :favorites_lower, :integer
	  add_column :searches, :favorites_upper, :integer
	end
  end