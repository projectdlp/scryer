class AddGenderToCharacter < ActiveRecord::Migration[4.2]
  def change
    add_column :characters, :isfemale, :boolean
  end
end
