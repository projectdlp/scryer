class AddNeedsReindexToCrawlBookmarks < ActiveRecord::Migration[5.1]
  def change
    add_column :crawl_bookmarks, :needs_reindex, :bool
    add_column :crawl_bookmarks, :last_reindexed, :timestamp
  end
end
