class AddOriginToQueue < ActiveRecord::Migration[6.0]
  def change
    add_column :queue_entries, :origin, :string
  end
end
