class AddFandomCategoryToFandom < ActiveRecord::Migration[4.2]
  def change
    add_column :fandoms, :category_id, :int
  end
end
