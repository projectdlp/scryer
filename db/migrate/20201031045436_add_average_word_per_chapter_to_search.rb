class AddAverageWordPerChapterToSearch < ActiveRecord::Migration[6.0]
  def change
    add_column :searches, :average_words_per_chapter_lower, :integer
  end
end
