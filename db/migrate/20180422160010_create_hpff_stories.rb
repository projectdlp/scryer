class CreateHpffStories < ActiveRecord::Migration[5.1]
  def change
    create_table :hpff_stories do |t|
      t.integer :hpff_id
      t.string :title
      t.string :author
      t.integer :author_id
      t.string :pre_data
      t.string :post_data
      t.string :summary

      t.timestamps
    end
  end
end
