class CreateHiddenStories < ActiveRecord::Migration[5.1]
  def change
    create_table :hidden_stories do |t|
      t.references :user, foreign_key: true
      t.references :story, foreign_key: true

      t.timestamps
    end
  end
end
