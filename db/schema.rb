# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_08_07_053259) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "btree_gin"
  enable_extension "plpgsql"

  create_table "announcements", id: :serial, force: :cascade do |t|
    t.text "message"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["starts_at", "ends_at"], name: "index_announcements_on_starts_at_and_ends_at"
  end

  create_table "authors", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "characters", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "fandom_id"
    t.boolean "isfemale"
    t.index ["fandom_id"], name: "index_characters_on_fandom_id"
  end

  create_table "crawl_bookmarks", primary_key: "fandoms", id: :integer, array: true, default: nil, force: :cascade do |t|
    t.datetime "last_crawled"
    t.boolean "bootstrapped"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "needs_reindex"
    t.datetime "last_reindexed"
    t.index ["bootstrapped"], name: "index_crawl_bookmarks_on_bootstrapped"
  end

  create_table "fandom_categories", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fandoms", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "category_id"
    t.boolean "is_indexed", default: false
    t.index ["is_indexed"], name: "index_fandoms_on_is_indexed"
  end

  create_table "favorites", id: :serial, force: :cascade do |t|
    t.integer "story_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["story_id", "user_id"], name: "index_favorites_on_story_id_and_user_id", unique: true
    t.index ["story_id"], name: "index_favorites_on_story_id"
    t.index ["user_id"], name: "index_favorites_on_user_id"
  end

  create_table "feedback", id: :serial, force: :cascade do |t|
    t.text "feedback_text"
    t.integer "score"
    t.integer "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hidden_authors", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_hidden_authors_on_author_id"
    t.index ["user_id"], name: "index_hidden_authors_on_user_id"
  end

  create_table "hidden_stories", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "story_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["story_id"], name: "index_hidden_stories_on_story_id"
    t.index ["user_id"], name: "index_hidden_stories_on_user_id"
  end

  create_table "hpff_chapters", force: :cascade do |t|
    t.integer "story_id"
    t.integer "chapter_id"
    t.string "chapter_name"
    t.string "chapter_blurb"
    t.integer "wordcount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hpff_stories", force: :cascade do |t|
    t.integer "hpff_id"
    t.string "title"
    t.string "author"
    t.integer "author_id"
    t.string "pre_data"
    t.string "post_data"
    t.string "summary"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "fetch_status"
    t.text "content"
    t.index ["hpff_id"], name: "hpff_id_unique", unique: true
  end

  create_table "languages", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pensieve_events", id: :serial, force: :cascade do |t|
    t.integer "event_id"
    t.string "event_name"
    t.string "url"
    t.string "ip"
    t.string "user_agent"
    t.integer "user_id"
    t.integer "story_id"
    t.string "story_name"
    t.integer "author_id"
    t.string "author_name"
    t.integer "chapter_id"
    t.string "chapter_name"
    t.integer "chapter_wordcount"
    t.datetime "page_load"
    t.datetime "page_start"
    t.datetime "page_exit"
    t.jsonb "raw_event"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["event_name"], name: "index_pensieve_events_on_event_name"
    t.index ["story_id", "chapter_id"], name: "index_pensieve_events_on_story_id_and_chapter_id"
    t.index ["user_id", "story_id"], name: "index_pensieve_events_on_user_id_and_story_id"
    t.index ["user_id"], name: "index_pensieve_events_on_user_id"
  end

  create_table "queue_entries", force: :cascade do |t|
    t.bigint "story_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "origin"
    t.index ["story_id"], name: "index_queue_entries_on_story_id"
    t.index ["user_id", "story_id"], name: "idx_queue_user_story", unique: true
    t.index ["user_id"], name: "index_queue_entries_on_user_id"
  end

  create_table "saved_searches", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.text "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_saved_searches_on_user_id"
  end

  create_table "searches", id: :serial, force: :cascade do |t|
    t.integer "fandoms", array: true
    t.integer "crossovers", array: true
    t.boolean "include_source_fandoms", default: false
    t.text "title"
    t.text "author"
    t.text "summary"
    t.integer "category_required", array: true
    t.integer "category_optional", array: true
    t.boolean "category_optional_exclude", default: false
    t.integer "character_required", array: true
    t.integer "character_optional", array: true
    t.boolean "character_optional_exclude", default: false
    t.text "language"
    t.text "status"
    t.text "rating", array: true
    t.integer "wordcount_lower"
    t.integer "wordcount_upper"
    t.integer "chapters_lower"
    t.integer "chapters_upper"
    t.text "sort_by"
    t.text "order_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "saved_search_id"
    t.integer "tags_include_ids", default: [], null: false, array: true
    t.integer "tags_exclude_ids", default: [], null: false, array: true
    t.datetime "updated_before"
    t.datetime "updated_after"
    t.datetime "published_before"
    t.datetime "published_after"
    t.jsonb "relationships"
    t.string "read_filter"
    t.boolean "exclude_crossover_fandoms", default: false
    t.integer "favorites_lower"
    t.integer "favorites_upper"
    t.integer "average_words_per_chapter_lower"
    t.index ["saved_search_id"], name: "index_searches_on_saved_search_id"
  end

  create_table "stories", id: :serial, force: :cascade do |t|
    t.string "title", null: false
    t.integer "author_id", null: false
    t.text "summary", null: false
    t.datetime "published", null: false
    t.datetime "updated", null: false
    t.datetime "last_seen", null: false
    t.string "status", null: false
    t.string "language", null: false
    t.string "rated", null: false
    t.integer "chapters", null: false
    t.integer "favs", null: false
    t.integer "follows", null: false
    t.integer "reviews", null: false
    t.integer "words", null: false
    t.integer "categories", null: false, array: true
    t.jsonb "relationships", null: false
    t.integer "characters", null: false, array: true
    t.integer "fandoms", null: false, array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "tags", default: [], null: false, array: true
    t.index ["author_id"], name: "index_stories_on_author_id"
    t.index ["categories"], name: "index_stories_on_categories", using: :gin
    t.index ["characters"], name: "index_stories_on_characters", using: :gin
    t.index ["fandoms"], name: "index_stories_on_fandoms", using: :gin
    t.index ["published"], name: "index_stories_on_published"
    t.index ["relationships"], name: "index_stories_on_relationships", using: :gin
    t.index ["updated"], name: "index_stories_on_updated"
    t.index ["words"], name: "index_stories_on_words"
  end

  create_table "story_clicks", id: :serial, force: :cascade do |t|
    t.integer "story_id", null: false
    t.integer "story_update_id"
    t.integer "user_id"
    t.jsonb "search"
    t.integer "page", null: false
    t.inet "ip"
    t.string "user_agent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "origin"
    t.index ["story_id", "user_id"], name: "index_story_clicks_on_story_id_and_user_id"
    t.index ["story_id"], name: "index_story_clicks_on_story_id"
    t.index ["user_id"], name: "index_story_clicks_on_user_id"
  end

  create_table "story_to_thread", id: false, force: :cascade do |t|
    t.integer "story_id"
    t.integer "thread_id"
    t.integer "vote_count"
    t.integer "vote_total"
    t.index ["story_id", "thread_id"], name: "index_story_to_thread_on_story_id_and_thread_id", unique: true
  end

  create_table "story_updates", id: :serial, force: :cascade do |t|
    t.bigint "story_id", null: false
    t.text "story_title", null: false
    t.bigint "author_id", null: false
    t.text "author_name", null: false
    t.integer "fandoms", array: true
    t.jsonb "update_contents", null: false
    t.datetime "updated_at", default: -> { "now()" }, null: false
    t.datetime "crawled_at", default: -> { "now()" }, null: false
    t.index ["author_id"], name: "idx_author_id"
    t.index ["story_id"], name: "idx_story_id"
    t.index ["updated_at"], name: "idx_updated_at", order: :desc
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name", limit: 20, null: false
    t.boolean "system", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "username"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "auth_token"
    t.integer "dlp_id"
    t.string "origin", default: "registration"
    t.integer "queue_entries_count", default: 0
    t.index ["auth_token"], name: "index_users_on_auth_token"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["dlp_id"], name: "index_users_on_dlp_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["email"], name: "users_email_key", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
    t.index ["username"], name: "users_username_key", unique: true
  end

  add_foreign_key "favorites", "users"
  add_foreign_key "hidden_authors", "authors"
  add_foreign_key "hidden_authors", "users"
  add_foreign_key "hidden_stories", "stories"
  add_foreign_key "hidden_stories", "users"
  add_foreign_key "queue_entries", "users"
end
