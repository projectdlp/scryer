module Scryer
  module Tagging
    module ContentTagger
      # @param [Story] story
      # @param [Array<Array<Character>] relationships
      def self.generate_story_tags(story, relationships = nil)
        tags = []

        relationships ||= story.relationships

        if story.summary =~ /multi\-pairings|(?:\/|x)\s?multi/i || relationships.any? { |r| r.size == 3 }
          if story.summary.count('/') >= 3
            tags << :harem
          else
            tags << :multi
          end
        end

        if story.summary =~ /harem/i || relationships.any? { |r| r.size > 3 }
          tags << :harem
        end

        if story.summary =~ /(^|\s|\*|\(|\/|pre-)slash|mpreg|yaoi|m\/m|\bhp\/?dm|\bhphp\/?ss|\bhptr|\bdrarry/i && story.summary !~ /no(?:t)?(?: a)? slash/i
          tags << :slash
        end

        # Gender flips
        # Find all Fem!<Foo> prefixes
        characters_with_tags = self.extract_character_tags(story)

        relationships.each do |group|
          genders = group.inject(Hash.new(0)) do |h, character|

            gender = character.gender

            # May be overridden by tags.
            if characters_with_tags[character].include?(:male)
              gender = :male
            elsif characters_with_tags[character].include?(:female)
              gender = :female
            end

            h[gender] += 1
            h
          end

          tags << :slash if genders[:male] > 1
          tags << :femslash if genders[:female] > 1
        end

        if story.summary =~ /fem(me)?(\s|!)?slash/i && story.summary !~ /no(?:t)?(?: a)? fem(me)?(\s|!)?slash/i
          tags << :femslash
        end

        tags << :gender_bender if story.summary =~ /fem(?:\s)?harry|fem! harry/i

        story.characters.each do |character|
          if characters_with_tags[character].include?(:male) && character.gender == :female
            tags << :gender_bender
          elsif characters_with_tags[character].include?(:female) && character.gender == :male
            tags << :gender_bender
          end
        end

        tags.uniq
      end

      # @param [Story] story
      # @return [Hash<Character, Set<Symbol>>]
      def self.extract_character_tags(story)
        tag_candidates = story.summary.gsub(/\w+(?:!\w+){1,3}/).map { Regexp.last_match[0] }
        character_tags = tag_candidates.map do |tag_string|
          parts = tag_string.split('!')

          c = story.characters.find { |c| c.name.downcase.starts_with?(parts.last.downcase) }
          if c
            {
                character: c,
                tags: parts[0..-2].map { |p| rewrite_tags(p.downcase).to_sym }
            }
          else
            nil
          end
        end.compact

        character_tags.inject(Hash.new { |h, k| h[k] = Set.new }) do |map, c|
          map[c[:character]].merge(c[:tags])
          map
        end
      end

      def self.rewrite_tags(tag)
        if tag.starts_with?('fem')
          'female'
        elsif tag == 'guy'
          'male'
        elsif tag == 'gray'
          'grey'
        else
          tag
        end
      end
    end
  end
end
