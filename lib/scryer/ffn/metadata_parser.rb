# frozen_string_literal: true

require 'virtus'
require 'pp'

module Scryer
  module FFN
    module MetadataParser
      @translations = Character.where("name LIKE '%,%' OR name LIKE '% - %'")
                               .map { |c| [c.name, c.name.gsub('-', '_').gsub(',', ';')] }
                               .to_h
                               .freeze
      @categories = Set.new(Category.all_cached.map(&:name)).freeze
      @languages = Language.all_cached.to_a.index_by(&:name).freeze

      # @param [String] info_string
      # @param [Array<Integer>] fandoms
      # @return [Scryer::FFN::Metadata]
      def self.parse_info_string(info_string, fandoms)
        # Translations to make
        # Calendar, J.
        # Jenny - Doctor's Daughter

        # Split on ' - '

        translated_str = info_string
        @translations.each do |k, v|
          translated_str.gsub!(k, v)
        end

        m = Metadata.new
        info_string.split(' - ').each do |part|
          extract_part(fandoms, m, part)
        end

        # Find status

        m
      end

      def self.parse_num(part)
        part.split(': ')[1].gsub(',', '').gsub('"', '').strip
      end

      def self.lookup_characters(character_names, fandoms)
        Character.where(name: character_names, fandom_id: fandoms).to_a
      end

      private

      # @param [Array<Integer>] fandoms
      # @param [Scryer::FFN::Metadata] m
      # @param [String] info_string_part
      def self.extract_part(fandoms, m, info_string_part)
        if info_string_part.match(/Chapters: /)
          m.chapters = parse_num(info_string_part)
        elsif info_string_part.match(/Favs: /)
          m.favs = parse_num(info_string_part)
        elsif info_string_part.match(/Follows: /)
          m.follows = parse_num(info_string_part)
        elsif info_string_part.match(/Rated: /)
          m.rated = info_string_part.split(': ')[1].gsub('+', 'PLUS').gsub('Fiction ', '').strip
        elsif info_string_part.match(/Reviews: /)
          m.reviews = parse_num(info_string_part)
        elsif info_string_part.match(/Words: /)
          m.words = parse_num(info_string_part)
        elsif info_string_part.match(/Published:/)
          # ignore
        elsif info_string_part.match(/Updated:/)
          # ignore
        elsif info_string_part.match(/id:/)
          # ignore
        elsif info_string_part == 'In-Progress'
          m.status = info_string_part
        elsif info_string_part == 'Complete'
          m.status = info_string_part
        elsif info_string_part == 'Status: Complete'
          m.status = info_string_part.gsub('Status: ', '').strip
        elsif @languages.key?(info_string_part)
          m.language = info_string_part
        elsif m.categories.empty? && m.words.zero? && @categories.any? { |c| info_string_part.include? c }
          extract_category(m, info_string_part)
        else
          extract_characters(fandoms, m, info_string_part)
        end
      end

      # @param [Scryer::FFN::Metadata] m
      # @param [String] info_string_part
      def self.extract_category(m, info_string_part)
        p = info_string_part.gsub('Hurt/Comfort', 'HC')
        category_names = p.split('/')
        category_names = category_names.map { |c| c.gsub('HC', 'Hurt/Comfort') }
        category_list = Category.where(name: category_names).to_a
        categories_by_name = category_list.group_by(&:name)
        m.categories = category_names.map { |category| categories_by_name[category][0].attributes }
      end

      # @param [Array<Integer>] fandoms
      # @param [Scryer::FFN::Metadata] m
      # @param [String] info_string_part
      def self.extract_characters(fandoms, m, info_string_part)
        # Grab the character names from either Name, Name or [Name, Name] blocks.
        character_names = info_string_part.split(/[\[\],]/).map(&:strip).reject(&:empty?)

        # Untranslate names
        character_names = character_names.map do |name|
          @translations.each do |k, v|
            name.gsub!(v, k)
          end

          name
        end

        # Zhang Fei is a FFN glitch, character id 65535. Neato.
        character_names.reject! { |c| c == 'Zhang Fei' }

        characters = lookup_characters(character_names, fandoms)
        characters_by_name = characters.group_by(&:name)
        characters = character_names.map do |character|
          clist = characters_by_name[character]

          raise "Could not find character: #{character}" if clist.nil?

          clist[0]
        end

        m.characters = characters.map(&:attributes)

        characters_by_name = characters.group_by(&:name)
        pairings = info_string_part.scan(/\[[^\]]+\]/).map do |pair|
          pair.split(/[\[\],]/).map(&:strip).reject(&:empty?)
        end.map do |names|
          {characters: names.map do |name|
            @translations.each do |k, v|
              name.gsub!(v, k)
            end

            characters_by_name[name][0].attributes
          end}
        end

        m.relationships = pairings
      end
    end

    class MetaCategory
      include Virtus.model

      attribute :id, Integer
      attribute :name, String

      def ==(other)
        self.id == other.id &&
            self.name == other.name
      end
    end

    class MetaCharacter
      include Virtus.model

      attribute :id, Integer
      attribute :name, String

      def ==(other)
        self.id == other.id &&
        self.name == other.name
      end
    end

    class MetaRelationship
      include Virtus.model

      attribute :characters, Array[MetaCharacter]

      def ==(other)
        self.characters == other.characters
      end
    end

    class Metadata
      include Virtus.model

      # status, characters, relationships, rated, wordcount, reviews, favs, follows, language
      attribute :categories, Array[MetaCategory], default: []
      attribute :chapters, Integer
      attribute :characters, Array[MetaCharacter], default: []
      attribute :favs, Integer, default: 0
      attribute :follows, Integer, default: 0
      attribute :language, String, default: 'English'
      attribute :status, String, default: 'In-Progress'
      attribute :tags, Array[String], default: []
      attribute :rated, String
      attribute :relationships, Array[MetaRelationship], default: []
      attribute :reviews, Integer, default: 0
      attribute :words, Integer, default: 0

      def ==(other)
        self.categories == other.categories &&
        self.chapters == other.chapters &&
        self.characters == other.characters &&
        self.favs == other.favs &&
        self.follows == other.follows &&
        self.language == other.language &&
        self.status == other.status &&
        self.tags == other.tags &&
        self.rated == other.rated &&
        self.relationships == other.relationships &&
        self.reviews == other.reviews &&
        self.words == other.words
      end


    end
  end
end
