module Scryer
  class Constants
    ORIGIN = %w(author_page recommendations search queue pensieve_import).freeze
    ORDERING = {'Descending' => 'desc', 'Ascending' => 'asc'}.freeze
    SORTING = {
      'Best Match' => '_score',
      'Updated' => 'updated',
      'Published' => 'published',
      'Wordcount' => 'meta.words',
      'Reviews' => 'meta.reviews',
      'Favorites' => 'meta.favs',
      'Follows' => 'meta.follows',
      'DLP Review Score' => '_dlp',
      'Popular & Recent' => '_popular',
      'Popular & Recent (Experimental)' => '_popular_exp',
      'Long & Recent' => '_long_recent',
      #'Flesch Index (Reading level)' => '_flesch',
      'Chapters' => 'meta.chapters'
    }.freeze
    RATING = {
      'K' => 'k',
      'K+' => 'kplus',
      'T' => 't',
      'M' => 'm',
    }.freeze
    READ_FILTER = {
      'Include All Stories' => 'all',
      'Exclude All Opened' => 'exclude_all_read',
      'Exclude Opened Without Updates' => 'exclude_read_without_updates',
    }.freeze
    STATUS = {
      '' => '',
      'Completed' => 'complete',
      'Hiatus' => 'hiatus',
      'In-Progress' => 'progress'
    }.freeze
    TAGS = ['slash', 'femslash', 'gender bender', 'harem', 'multi'].freeze
    BANNED_EMAIL_PATTERNS = [
        /.+\.in\.net/,
        /.+\.xyz/,
        /.+@rambler\.ru/,
        /.+@mail\.ru/,
        /.+@gmailbd\.ga/,
        /.+@mailserp\.com/,
        /.+@24mailpro\.top/,
        /.+@oturizme\.net/,
        /.+@kiyoakari\.xyz/,
        /.+@enersets\.com/,
        /mksorbplg\+.+/,
    ].freeze
  end
end
