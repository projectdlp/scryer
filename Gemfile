source 'https://rubygems.org'

gem 'activerecord-import'
gem 'autoprefixer-rails'
gem 'bootstrap-sass', '~> 3.3'
gem 'chosen-rails'
gem 'counter_culture', '~> 2.0'
gem 'connection_pool'
gem 'dalli'
gem 'devise'
gem 'devise-async'
gem 'dogstatsd-ruby'
gem 'elasticsearch'
gem 'faraday'
gem 'faraday_middleware', '~> 0.10.0'
gem 'fast_blank'
gem 'flamegraph'
gem 'font-awesome-rails'
gem 'foreman'
gem 'haml-rails'
gem 'hashids_rails', github: 'brianpetro/hashids_rails'
gem 'jbuilder', '~> 2.0'
gem 'jquery-rails'
gem 'jwt'
gem 'kaminari'
gem 'lograge' # We don't need huge multi-line messages...
gem 'newrelic_rpm'
gem 'pg'
gem 'rack-attack'
gem 'rack-mini-profiler'
gem 'rails', '~> 6'
gem 'rails-timeago', '~> 2.0'
gem 'rash'
gem 'record_tag_helper', '~> 1.0'
gem 'redis'
gem 'rollbar'
gem 'rollout'
gem 'sass-rails', '~> 5.0'
gem 'select2-rails'
gem 'sidekiq'
gem 'sidekiq-scheduler'
gem 'sinatra', require: nil
gem 'stackprof', '~> 0.2.7'
gem 'uglifier', '>= 1.3.0'
gem 'unicorn'
gem 'virtus'
gem 'webpacker', '~> 5'

# Use debugger
gem 'debugger2', group: [:development, :test]

group :test do
  gem 'factory_bot_rails'
  gem 'minitest-rails'
  gem 'minitest-reporters', '>= 0.5.0'
  gem 'mocha'
end

group :development do
  gem 'annotate'
  gem 'binding_of_caller' # better traces for better_errors
  gem 'bullet'
  gem 'letter_opener'
  gem 'listen'
  gem 'ruby-prof'
  gem 'spring'
  gem 'web-console'
end
